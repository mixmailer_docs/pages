.. _comparative:

Comparative remailer mix neworks, onion routing and pEp
-------------------------------------------------------

This is not an exhautive comparative and might not be accurate.

It is base on the pages:

- :ref:`type-i-cypherpunks`
- :ref:`type-ii-mixmaster`
- :ref:`type-iii-mixminion`
- :ref:`onion-routing`
- :ref:`p1`

+-----------+-----------+-----------+-----------+-----------+-----------+
|           | Remailer  | Mixnetwor |           | Onion     | pEp       |
|           |           | ks        |           | Routing   | MixMailer |
+-----------+-----------+-----------+-----------+-----------+-----------+
|           | Type      | Type      | Type      | Tor       |           |
|           | I/Cypherp | II/Mixmas | III/Mixmi |           |           |
|           | unks      | ter       | nion      |           |           |
+===========+===========+===========+===========+===========+===========+
| transport | SMTP      | SMTP      | TLS +     | TLS       | SMTP?     |
|           |           |           | SMTP?     |           |           |
+-----------+-----------+-----------+-----------+-----------+-----------+
| nodes key | NA        | yes       | yes       | yes       | yes       |
|           |           |           |           |           | (OpenPGP) |
|           |           |           |           |           | ?         |
+-----------+-----------+-----------+-----------+-----------+-----------+
| key       | NA        | no        | yes       | yes       | active/pa |
| rotation  |           |           |           |           | sive      |
+-----------+-----------+-----------+-----------+-----------+-----------+
| nodes/key | manual    | pinger?   | directory | dirauths  | GNS?      |
| discovery |           |           |           |           |           |
+-----------+-----------+-----------+-----------+-----------+-----------+
| types of  | NA        | same?     | several?  | “G,M,E”   | same?     |
| nodes     |           |           |           |           |           |
+-----------+-----------+-----------+-----------+-----------+-----------+
| packet    | ?         | ?         | Sphinx    | specific  | ?         |
| format    |           |           |           |           |           |
+-----------+-----------+-----------+-----------+-----------+-----------+
| packet    | different | same?     | same      | same      | different |
| size      |           |           |           |           | in        |
|           |           |           |           |           | future?   |
+-----------+-----------+-----------+-----------+-----------+-----------+
| packet    | NA        | yes       | yes       | no        | yes in    |
| mixing    |           |           |           |           | future?   |
+-----------+-----------+-----------+-----------+-----------+-----------+
| cover     | NA        | yes       | yes?      | no        | ?         |
| traffic   |           |           |           |           |           |
+-----------+-----------+-----------+-----------+-----------+-----------+
| directory | no        | no        | central   | decentral | GNS/trust |
| server    |           |           |           | ized      | ed        |
|           |           |           |           |           | nodes?    |
+-----------+-----------+-----------+-----------+-----------+-----------+
| reply     | no        | no        | SURB      | “yes,     | NA        |
|           |           |           |           | circuit”  |           |
+-----------+-----------+-----------+-----------+-----------+-----------+
| spam      | yes       | yes       | yes       | NA        | ?         |
+-----------+-----------+-----------+-----------+-----------+-----------+
| network   | no        | no        | no        | yes       | ?         |
| diversity |           |           |           |           |           |
+-----------+-----------+-----------+-----------+-----------+-----------+
| user      | no        | no        | no        | yes       | ?         |
| friendly  |           |           |           |           |           |
+-----------+-----------+-----------+-----------+-----------+-----------+
| old       | yes       | yes       | yes       | no        | ?         |
| crypto    |           |           |           |           |           |
+-----------+-----------+-----------+-----------+-----------+-----------+
